//
//  InputEiga.swift
//  drv
//
//  Created by け on 2020/08/17.
//  Copyright © 2020 ke. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftUI

struct BoxItemCreate03Scene: View {
    @State public var itemTitle3 = ""
    @State public var contentbox3 = ""
    @State var isActiveSubView = false
    @State var image: UIImage?
    @State public var inputSe:Int = 2
    @EnvironmentObject var datastore: DataStore
    
    public var realm = try! Realm()
    var eiga: Eiga!

    var body: some View {
        NavigationView {
            ZStack {
                Color(self.datastore.boxColor)
                    .edgesIgnoringSafeArea(.all)
           
                VStack {
                    Spacer()
                    HStack {
                        TextField("", text: $itemTitle3)
                            .padding(.vertical, 7.0)
                            .foregroundColor(Color.white)
                            .multilineTextAlignment(.center)
                            .onAppear {
                                self.itemTitle3 = "default text"
                            }
                    }
                                   
                    .background(Color("grayblack"))
                    Spacer()
                    Spacer()
                    ImagePickAndDisplaySwiftUIView2(inputSe:self.$inputSe)
                    Spacer()
                    HStack {
                        Spacer()
                                    
                        TextField("", text: $contentbox3)
                            .lineLimit(5)
                            .foregroundColor(Color.white)
                            .frame(width: 350, height: 160)
                            .background(Color("sukedark"))
                            .cornerRadius(12)
                        Spacer()
                    }
                                    
                   Spacer()
                }
            }
        }
    }
}

struct BoxItemCreate03Scene_Previews: PreviewProvider {
    static var previews: some View {
        BoxItemCreate03Scene().environmentObject(DataStore())
    }
}
