//
//  InputEiga.swift
//  drv
//
//  Created by け on 2020/08/17.
//  Copyright © 2020 ke. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftUI

struct BoxFaceCreateScene: View {
    @EnvironmentObject var datastore: DataStore
    // ボックスのタイトルを格納する変数
    @State public var boxTitle = ""
    // 遷移フラグ
    @State var isActiveSubView = false
    // ボックスの背景色を格納する変数
    @State var bckcolor = "BoxColor1"
    //  @EnvironmentObject var object: EigaModel
    public var realm = try! Realm()
    var box: Box!

    var body: some View {
                ZStack {
                    // 背景色を設定
                Color(self.datastore.boxColor)
                    .edgesIgnoringSafeArea(.all)
           
                VStack(spacing: 65) {
                   //Spacer()
                    HStack {
                        // ボックスタイトルの入力欄
                        TextField("", text: self.$datastore.boxTitle)
                            .frame(height: 40)
                            // ここでboxTitleのフォントと太さを変えたい！！
                            .padding(.vertical, 9.0)
                            // 入力される文字の色
                            .foregroundColor(Color.white)
                            // 文字を中央に配置
                            .multilineTextAlignment(.center)
                            // .onAppear → ビュー遷移時の処理
                            .onAppear {
                                self.datastore.boxTitle = "ボックス名を入力"
                            }
                    }
                    // ボックスタイトル入力欄の色指定
                    .background(Color("sukedark"))
                    //Spacer()
                    // ImagePickAndDisplaySwiftUIViewを呼び出している?
                    ImagePickAndDisplaySwiftUIView()
                    // ボックス画像欄とボックスカラー選択ボタンの間隔を調整
                    .padding(.init(top: 1,
                                   leading: 0,
                                   bottom: -40,
                                   trailing: 0))
                    
                    //Spacer()
                                    
                    HStack {
                        Group {
                            // ボックスカラーの選択ボタンの設定
                            // 左のカラー選択ボタン
                            Button(action: { self.datastore.boxColor = "BoxColor1"
                                self.bckcolor = "BoxColor1"
                            }) {
                                Rectangle()
                                    .fill(Color("BoxColor1"))
                                    .frame(width: 30, height: 30)
                                // 黒色の枠線を設定
                                .border(Color(.black), width: 1.5)
                            }
                            // 中央のカラー選択ボタン
                            Button(action: { self.datastore.boxColor = "BoxColor2"
                                self.bckcolor = "BoxColor2"
                            }) {
                                Rectangle()
                                    .fill(Color("BoxColor2"))
                                    .frame(width: 30, height: 30)
                                // 黒色の枠線を設定
                                .border(Color(.black), width: 1.5)
                            }
                            // 右のカラー選択ボタン
                            Button(action: { self.datastore.boxColor = "BoxColor3"
                                self.bckcolor = "BoxColor3"
                            }) {
                                Rectangle()
                                    .fill(Color("BoxColor3"))
                                    .frame(width: 30, height: 30)
                                // 黒色の枠線を設定
                                .border(Color(.black), width: 1.5)
                            }
                        }
                        
                    }
                                    
                  Spacer()
                }
                
            
        }
    }
}

struct BoxFaceCreateScene_Previews: PreviewProvider {
    static var previews: some View {
        BoxFaceCreateScene()
    }
}
