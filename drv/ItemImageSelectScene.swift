//
//  MovieImageSelect_Scene.swift
//  drv
//
//  Created by け on 2020/08/23.
//  Copyright © 2020 ke. All rights reserved.
//

import SwiftUI

struct ItemImageSelectScene: View {
    @EnvironmentObject var datastore: DataStore
   
    var body: some View {
        ZStack {
                //背景色の設定
                Color(self.datastore.boxColor)
                .edgesIgnoringSafeArea(.all)
            VStack {
                Text("イメージを選択してください")
                    .font(.body)
                    .foregroundColor(Color.white)
                    .padding()
                HStack {
                    //1つ目のアイテムイメージの選択欄
                    Button(action: {
                        self.datastore.boxItemImage[0] = "ItemImage1"}
                    ) {
                        Image("ItemImage1")
                            .resizable()
                            .renderingMode(.original)
                            .frame(width: 70, height: 100)
                            .shadow(color: .black, radius: 1, x: 2, y: 2)
                    }
                    //2つ目のアイテムイメージの選択欄
                    Button(action:  {
                        self.datastore.boxItemImage[0] = "ItemImage2"
                    }) {
                        Image("ItemImage2")
                            .resizable()
                            .renderingMode(.original)
                            .frame(width: 70, height: 100)
                            .shadow(color: .black, radius: 1, x: 2, y: 2)
                    }
                    //3つ目のアイテムイメージの選択欄
                    Button(action:  {
                        self.datastore.boxItemImage[0] = "ItemImage3"
                    }) {
                        Image("ItemImage3")
                            .resizable()
                            .renderingMode(.original)
                            .frame(width: 70, height: 100)
                            .shadow(color: .black, radius: 1, x: 2, y: 2)
                    }
                }
                HStack {
                    //4つ目のアイテムイメージの選択欄
                    Button(action:  {
                        self.datastore.boxItemImage[0] = "ItemImage4"
                    }) {
                        Image("ItemImage4")
                            .resizable()
                            .renderingMode(.original)
                            .frame(width: 70, height: 100)
                            .shadow(color: .black, radius: 1, x: 2, y: 2)
                    }
                    //5つ目のアイテムイメージの選択欄
                    Button(action: {
                        self.datastore.boxItemImage[0] = "ItemImage5"
                        
                    }) {
                        Image("ItemImage5")
                            .resizable()
                            .renderingMode(.original)
                            .frame(width: 70, height: 100)
                            .shadow(color: .black, radius: 1, x: 2, y: 2)
                    }
                    //6つ目のアイテムイメージの選択欄
                    Button(action: {
                        self.datastore.boxItemImage[0] = "ItemImage6"
                        
                    }) {
                        Image("ItemImage6")
                            .resizable()
                            .renderingMode(.original)
                            .frame(width: 70, height: 100)
                            .shadow(color: .black, radius: 1, x: 2, y: 2)
                    }
                }
            }
        }
    }
}



